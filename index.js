const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.cwrxp6r.mongodb.net/b244-to-do?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true,
}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", ()=>{console.log("We're connected to the database")});

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	}
});

const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

/*app.post("/signup", (req, res) => {
	if(!req.body.username || !req.body.password) {
		return res.status(400).json({ message: "Username or Password cannot be null"});
	}
	User.findOne({username: req.body.username}, (err, existingUser) => {
		if(existingUser) {
			return res.status(400).json({ message: "User already registered"});
		}
	const newUser = new User(req.body);
		newUser.save((err) => {
			if (err) {
				return res.status(400).json({ message: err });
			} else{
				return res.status(201).json({ message: "New user registered" });
			}
		});
	});
});*/


app.post("/signup", async (req, res) => {
	if(!req.body.username || !req.body.password) 
		return res.status(400).json({ message: "Username or Password cannot be blank"});

	if (await User.exists({username: req.body.username}))
		return res.status(400).json({ message: "User already registered"});

	await new User(req.body).save();
	res.status(201).json({ message: "New user registered" });
});

app.listen(port, () => {console.log(`Server running at port ${port}`)});